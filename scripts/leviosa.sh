# Please run with sudo.
# This bash will launch all the components of the system, in the order of:
# 0. px4 (mavros & gcs_bridge) 


# whether
bypass_pp=false

pp_fly_maxVel_xy=1.0
pp_fly_maxVel_z=1.0
pp_fly_maxVel_c=0.3

init_path=/home/leviosa/catkin_ws
export LC_ALL="en_US.UTF-8"

# delay between launching various modules
module_delay=1.5

# check whether is running as sudo
if [ "$EUID" -eq 0 ]
    then echo "Please DO NOT run as root."
    exit
fi

if [ "${STY}" != "" ]
    then echo "You are running the script in a screen environment. Please quit the screen."
    exit
fi

output="$(screen -ls)"
if [[ $output != *"No Sockets found"* ]]; then
    echo "There are some screen sessions alive. Please run 'pkill screen' before launching uavos."
    exit
fi

echo "The system is booting..."

# roscore
screen -d -m -S roscore bash -c "source devel/setup.bash; roscore; exec bash -i"
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
echo "roscore ready."

screen -d -m -S rosparam bash -c "source devel/setup.bash; source /opt/ros/kinetic/setup.bash; exec bash -i"
echo "rosparam ready."

# --------------------------------------------
# prepareation
cd ${init_path}
source devel/setup.bash

#################################################################################################################################
# -4 tf static transformation
screen -d -m -S static_tf bash -c "source devel/setup.bash; roslaunch nus_launch leviosa_static_tf.launch; exec bash -i"
sleep ${module_delay}
echo "static_tf ready."

#################################################################################################################################
# -3 Velodyne_driver
screen -d -m -S velodyne_driver bash -c "source devel/setup.bash; roslaunch velodyne_pointcloud VLP16_points.launch; exec bash -i"
sleep ${module_delay}
echo "velodyne_driver ready."

#################################################################################################################################
# -2 voxel_grid_filter
screen -d -m -S voxel_grid_filter bash -c "source devel/setup.bash; roslaunch nus_launch voxel_grid_filter.launch; exec bash -i"
sleep ${module_delay}
echo "voxel_grid_filter ready."

#################################################################################################################################
# -1 throttle_pointcloud
screen -d -m -S throttle_pointcloud bash -c "source devel/setup.bash; rosrun topic_tools throttle messages /velodyne_points_xz_filtered 10.0; exec bash -i"
sleep ${module_delay}
echo "throttle_pointcloud ready."

#################################################################################################################################
# 0. px4
screen -d -m -S px4 bash -c "source devel/setup.bash; roslaunch mavros px4.launch; exec bash -i"
sleep ${module_delay}
echo "px4 ready."

#################################################################################################################################
# 1. loam
screen -d -m -S loam bash -c "source devel/setup.bash; roslaunch loam_velodyne remote_loam_velodyne.launch; exec bash -i"
sleep ${module_delay}
echo "loam ready."

#################################################################################################################################
# 2. path_planning
screen -d -m -S path_planning bash -c "source devel/setup.bash; roslaunch nus_3d_pathplanning pathplanning.launch; exec bash -i"
sleep ${module_delay}
echo "path_planning ready."

#################################################################################################################################
# 3. ref_generator
screen -d -m -S reference_generator bash -c "source devel/setup.bash; roslaunch nus_reference_generator reference_generator.launch pp_fly_maxVel_xy:=${pp_fly_maxVel_xy} pp_fly_maxVel_z:=${pp_fly_maxVel_z} pp_fly_maxVel_c:=${pp_fly_maxVel_c}; exec bash -i"
sleep ${module_delay}
echo "reference_generator ready."

#################################################################################################################################
# 4. task_manager
screen -d -m -S task_manager bash -c "source devel/setup.bash; roslaunch nus_task_manager task_manager.launch bypass_pp:=${bypass_pp}; exec bash -i"
sleep ${module_delay}
echo "task_manager ready."

#################################################################################################################################
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}
sleep ${module_delay}

# 5. nus_laser_map
screen -d -m -S nus_laser_map bash -c "source devel/setup.bash; roslaunch octomap_server leviosa_octomap_mapping_local.launch; exec bash -i"
sleep ${module_delay}
echo "nus_laser_map ready."

#################################################################################################################################
# 6. rosbag
screen -d -m -S rosbag bash -c "source devel/setup.bash; roslaunch nus_launch leviosa_log.launch; exec bash -i"
sleep ${module_delay}
echo "rosbag ready."

echo "System is started."

